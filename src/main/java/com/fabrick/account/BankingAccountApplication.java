package com.fabrick.account;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.fabrick.account"})
@OpenAPIDefinition(servers = {@Server(url = "${server.servlet.context-path}", description = "Default Server URL")})
public class BankingAccountApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BankingAccountApplication.class, args);
	}

	@Override
	public void run(String... arg0) {

	}

}

