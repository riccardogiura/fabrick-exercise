
package com.fabrick.account.services;


import com.fabrick.account.common.client.AccountApiFacade;
import com.fabrick.account.common.itf.Operation;
import com.fabrick.account.common.model.CreateMoneyTransferResponse;
import com.fabrick.account.dto.CreateMoneyTransferRequestDto;
import org.springframework.stereotype.Service;


@Service("createMoneyTransferServiceBean")
public class CreateMoneyTransferServiceBean implements Operation<CreateMoneyTransferRequestDto, CreateMoneyTransferResponse, Exception> {

    private final AccountApiFacade accountApiFacade;

    public CreateMoneyTransferServiceBean(AccountApiFacade accountApiFacade) {
        this.accountApiFacade = accountApiFacade;
    }

    @Override
    public CreateMoneyTransferResponse execute(CreateMoneyTransferRequestDto input) throws Exception {
        return accountApiFacade.createMoneyTransfer(input.getAccountId(), input.getRequest()).getPayload();
    }

}

