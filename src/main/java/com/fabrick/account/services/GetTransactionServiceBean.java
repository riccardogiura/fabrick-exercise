
package com.fabrick.account.services;


import com.fabrick.account.common.client.AccountApiFacade;
import com.fabrick.account.common.itf.Operation;
import com.fabrick.account.common.model.TransactionItem;
import com.fabrick.account.dto.GetTransactionsRequestDto;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("getTransactionServiceBean")
public class GetTransactionServiceBean implements Operation<GetTransactionsRequestDto, List<TransactionItem>, Exception> {

    private final AccountApiFacade accountApiFacade;

    public GetTransactionServiceBean(AccountApiFacade accountApiFacade) {
        this.accountApiFacade = accountApiFacade;
    }

    @Override
    public List<TransactionItem> execute(GetTransactionsRequestDto input) throws Exception {
        return accountApiFacade.getTransactions(input.getAccountId(), input.getFromAccountingDate(), input.getToAccountingDate()).getPayload().getList();
    }

}

