
package com.fabrick.account.services;


import com.fabrick.account.common.client.AccountApiFacade;
import com.fabrick.account.common.itf.Operation;
import com.fabrick.account.common.model.BalanceResponse;
import org.springframework.stereotype.Service;


@Service("getBalanceServiceBean")
public class GetBalanceServiceBean implements Operation<String, BalanceResponse, Exception> {

    private final AccountApiFacade accountApiFacade;

    public GetBalanceServiceBean(AccountApiFacade accountApiFacade) {
        this.accountApiFacade = accountApiFacade;
    }

    @Override
    public BalanceResponse execute(String accountId) throws Exception {
        return accountApiFacade.getBalance(accountId).getPayload();
    }

}

