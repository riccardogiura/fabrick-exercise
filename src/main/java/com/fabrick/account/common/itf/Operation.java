
package com.fabrick.account.common.itf;

public interface Operation<I, O, E extends Exception> {

    public O execute(I input) throws E, Exception;
}
