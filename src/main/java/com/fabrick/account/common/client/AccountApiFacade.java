package com.fabrick.account.common.client;


import com.fabrick.account.common.model.CreateMoneyTransferRequest;
import com.fabrick.account.common.model.fabrickresponse.FabrickBalanceResponse;
import com.fabrick.account.common.model.fabrickresponse.FabrickCreateMoneyTransferResponse;
import com.fabrick.account.common.model.fabrickresponse.FabrickTransactionsResponse;
import com.fabrick.account.utils.configuration.MicroserviceProperties;
import com.fabrick.account.utils.configuration.restclient.RestClientHttpInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;


@Service
public class AccountApiFacade {
    private final RestTemplate restTemplate;

    @Autowired
    private MicroserviceProperties microserviceProperties;

    private static final Logger log = LoggerFactory.getLogger(AccountApiFacade.class);


    public AccountApiFacade(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public FabrickBalanceResponse getBalance(String accountId) {
        String url = UriComponentsBuilder.fromHttpUrl(microserviceProperties.getFabrickBalanceUri()).buildAndExpand(accountId).toUriString();
        HttpHeaders httpHeaders = createHeaders(url);
        HttpEntity<Void> request = new HttpEntity<>(httpHeaders);
        return restTemplate.exchange(url, HttpMethod.GET, request, FabrickBalanceResponse.class).getBody();
    }

    public FabrickTransactionsResponse getTransactions(String accountId, String fromAccountingDate, String toAccountingDate) {
        String url = UriComponentsBuilder.fromHttpUrl(microserviceProperties.getFabrickTransactionsUri())
                .queryParam("fromAccountingDate", fromAccountingDate)
                .queryParam("toAccountingDate", toAccountingDate)
                .buildAndExpand(accountId).toUriString();
        HttpHeaders httpHeaders = createHeaders(url);
        HttpEntity<Void> request = new HttpEntity<>(httpHeaders);
        return restTemplate.exchange(url, HttpMethod.GET, request, FabrickTransactionsResponse.class).getBody();
    }

    public FabrickCreateMoneyTransferResponse createMoneyTransfer(String accountId, CreateMoneyTransferRequest body) {
        String url = UriComponentsBuilder.fromHttpUrl(microserviceProperties.getFabrickMoneyTransfersUri()).buildAndExpand(accountId).toUriString();;
        HttpHeaders httpHeaders = createHeaders(url);
        HttpEntity<CreateMoneyTransferRequest> request = new HttpEntity<>(body, httpHeaders);
        return restTemplate.postForObject(url, request, FabrickCreateMoneyTransferResponse.class);
    }

    private HttpHeaders createHeaders(String url) {
        HttpHeaders headers = new HttpHeaders();
        try {
            headers.set("host", getHost(url));
        } catch (URISyntaxException e) {
            log.error("Error generating host header. Cause is: " + e.getLocalizedMessage());
        }

        headers.set("Api-Key", microserviceProperties.getFabrickApiKey());
        headers.set("Auth-Schema", microserviceProperties.getFabrickAuthSchema());
        headers.set("X-Time-Zone", microserviceProperties.getFabrickTimeZone());

        return headers;
    }

    private String getHost(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

}
