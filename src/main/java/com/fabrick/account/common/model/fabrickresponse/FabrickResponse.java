package com.fabrick.account.common.model.fabrickresponse;

import java.util.ArrayList;
import java.util.List;

public class FabrickResponse<T> {

    private String status;
    private List<FabrickApiError> errors = new ArrayList<>();
    private T payload;

    // Costruttori, getter e setter

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FabrickApiError> getErrors() {
        return errors;
    }

    public void setErrors(List<FabrickApiError> errors) {
        this.errors = errors;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
