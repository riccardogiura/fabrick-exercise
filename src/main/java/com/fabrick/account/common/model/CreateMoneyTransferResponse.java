package com.fabrick.account.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class CreateMoneyTransferResponse {
    @JsonProperty("moneyTransferId")
    private String moneyTransferId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("direction")
    private String direction;

    @JsonProperty("creditor")
    private Account creditor;

    @JsonProperty("debtor")
    private Account debtor;

    @JsonProperty("cro")
    private String cro;

    @JsonProperty("uri")
    private String uri;

    @JsonProperty("trn")
    private String trn;

    @JsonProperty("description")
    private String description;

    @JsonProperty("createdDatetime")
    private String createdDatetime;

    @JsonProperty("accountedDatetime")
    private String accountedDatetime;

    @JsonProperty("debtorValueDate")
    private String debtorValueDate;

    @JsonProperty("creditorValueDate")
    private String creditorValueDate;

    @JsonProperty("amount")
    private Amount amount;

    @JsonProperty("isUrgent")
    private boolean isUrgent;

    @JsonProperty("isInstant")
    private boolean isInstant;

    @JsonProperty("feeType")
    private String feeType;

    @JsonProperty("feeAccountId")
    private String feeAccountId;

    @JsonProperty("fees")
    private List<Fee> fees;

    @JsonProperty("hasTaxRelief")
    private boolean hasTaxRelief;

    // Classe Account
    static class Account {
        private String name;
        private AccountDetails account;


        // Classe AccountDetails
        static class AccountDetails {
            private String accountCode;
            private String bicCode;

        }
    }

    // Classe Amount
    static class Amount {
        private BigDecimal debtorAmount;
        private String debtorCurrency;
        private BigDecimal creditorAmount;
        private String creditorCurrency;
        private String creditorCurrencyDate;
        private BigDecimal exchangeRate;

    }

    // Classe Fee
    static class Fee {
        private String feeCode;
        private String description;
        private BigDecimal amount;
        private String currency;

    }
}
