package com.fabrick.account.common.model.fabrickresponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FabrickApiError {

    @JsonProperty("code")
    private String code;
    @JsonProperty("description")
    private String description;
    @JsonProperty("params")
    private String params;


}
