package com.fabrick.account.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * TransactionType
 */
@Validated


public class TransactionType {
    @JsonProperty("enumeration")
    private String enumeration = null;

    @JsonProperty("value")
    private String value = null;

    public TransactionType enumeration(String enumeration) {
        this.enumeration = enumeration;
        return this;
    }

    /**
     * Get enumeration
     *
     * @return enumeration
     **/
    @Schema(required = true, description = "")
    @NotNull

    public String getEnumeration() {
        return enumeration;
    }

    public void setEnumeration(String enumeration) {
        this.enumeration = enumeration;
    }

    public TransactionType value(String value) {
        this.value = value;
        return this;
    }

    /**
     * Get value
     *
     * @return value
     **/
    @Schema(required = true, description = "")
    @NotNull

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransactionType transactionType = (TransactionType) o;
        return Objects.equals(this.enumeration, transactionType.enumeration) &&
                Objects.equals(this.value, transactionType.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enumeration, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class TransactionType {\n");

        sb.append("    enumeration: ").append(toIndentedString(enumeration)).append("\n");
        sb.append("    value: ").append(toIndentedString(value)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
