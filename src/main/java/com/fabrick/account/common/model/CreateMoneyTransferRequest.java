package com.fabrick.account.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CreateMoneyTransferRequest {
    @JsonProperty("creditor")
    private Creditor creditor;

    @JsonProperty("description")
    private String description;

    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("executionDate")
    private String executionDate;

    @JsonProperty("uri")
    private String uri;

    @JsonProperty("isUrgent")
    private boolean isUrgent;

    @JsonProperty("isInstant")
    private boolean isInstant;

    @JsonProperty("feeType")
    private String feeType;

    @JsonProperty("feeAccountId")
    private String feeAccountId;

    @JsonProperty("taxRelief")
    private TaxRelief taxRelief;

    static class Creditor {
        @JsonProperty("name")
        private String name;

        @JsonProperty("account")
        private CreditorAccount account;

        @JsonProperty("address")
        private CreditorAddress address;
    }

    static class CreditorAccount {
        @JsonProperty("accountCode")
        private String accountCode;

        @JsonProperty("bicCode")
        private String bicCode;
    }

    static class CreditorAddress {
        @JsonProperty("address")
        private String address;

        @JsonProperty("city")
        private String city;

        @JsonProperty("countryCode")
        private String countryCode;
    }

    static class TaxRelief {
        @JsonProperty("taxReliefId")
        private String taxReliefId;

        @JsonProperty("isCondoUpgrade")
        private boolean isCondoUpgrade;

        @JsonProperty("creditorFiscalCode")
        private String creditorFiscalCode;

        @JsonProperty("beneficiaryType")
        private String beneficiaryType;

        @JsonProperty("naturalPersonBeneficiary")
        private NaturalPersonBeneficiary naturalPersonBeneficiary;

        @JsonProperty("legalPersonBeneficiary")
        private LegalPersonBeneficiary legalPersonBeneficiary;
    }

    static class NaturalPersonBeneficiary {
        @JsonProperty("fiscalCode1")
        private String fiscalCode1;
    }

    static class LegalPersonBeneficiary {
        @JsonProperty("fiscalCode")
        private String fiscalCode;

        @JsonProperty("legalRepresentativeFiscalCode")
        private String legalRepresentativeFiscalCode;
    }
}