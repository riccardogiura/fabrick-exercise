package com.fabrick.account.common.model;

import com.fabrick.account.common.model.fabrickresponse.FabrickApiError;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ApiError {

    @JsonProperty("status")
    private HttpStatus status = null;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp = null;
    @JsonProperty("errors")
    private List<FabrickApiError> errors = new ArrayList<>();


    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus status, List<FabrickApiError> errors) {
        this();
        this.status = status;
        this.errors = errors;
    }
}