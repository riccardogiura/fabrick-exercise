package com.fabrick.account.common.model;

import java.time.LocalDate;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;


/**
 * TransactionResponse
 */
@Validated


public class TransactionItem {
  @JsonProperty("transactionId")
  private String transactionId = null;

  @JsonProperty("operationId")
  private String operationId = null;

  @JsonProperty("accountingDate")
  private LocalDate accountingDate = null;

  @JsonProperty("valueDate")
  private LocalDate valueDate = null;

  @JsonProperty("type")
  @Valid
  private TransactionType type = null;

  @JsonProperty("amount")
  private BigDecimal amount = null;

  @JsonProperty("currency")
  private String currency = null;

  @JsonProperty("description")
  private String description = null;

  public TransactionItem transactionId(String transactionId) {
    this.transactionId = transactionId;
    return this;
  }

  /**
   * Get transactionId
   * @return transactionId
   **/
  @Schema(example = "1331714087", required = true, description = "")
      @NotNull

    public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public TransactionItem operationId(String operationId) {
    this.operationId = operationId;
    return this;
  }

  /**
   * Get operationId
   * @return operationId
   **/
  @Schema(example = "273015", required = true, description = "")
      @NotNull

    public String getOperationId() {
    return operationId;
  }

  public void setOperationId(String operationId) {
    this.operationId = operationId;
  }

  public TransactionItem accountingDate(LocalDate accountingDate) {
    this.accountingDate = accountingDate;
    return this;
  }

  /**
   * Get accountingDate
   * @return accountingDate
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public LocalDate getAccountingDate() {
    return accountingDate;
  }

  public void setAccountingDate(LocalDate accountingDate) {
    this.accountingDate = accountingDate;
  }

  public TransactionItem valueDate(LocalDate valueDate) {
    this.valueDate = valueDate;
    return this;
  }

  /**
   * Get valueDate
   * @return valueDate
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public LocalDate getValueDate() {
    return valueDate;
  }

  public void setValueDate(LocalDate valueDate) {
    this.valueDate = valueDate;
  }

  public TransactionItem type(TransactionType type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
   **/
  @Schema(required = true, description = "")
      @NotNull
    @Valid
    public TransactionType getType() {
    return type;
  }

  public void setType(TransactionType type) {
    this.type = type;
  }

  public TransactionItem amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public TransactionItem currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public TransactionItem description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionItem transactionItem = (TransactionItem) o;
    return Objects.equals(this.transactionId, transactionItem.transactionId) &&
        Objects.equals(this.operationId, transactionItem.operationId) &&
        Objects.equals(this.accountingDate, transactionItem.accountingDate) &&
        Objects.equals(this.valueDate, transactionItem.valueDate) &&
        Objects.equals(this.type, transactionItem.type) &&
        Objects.equals(this.amount, transactionItem.amount) &&
        Objects.equals(this.currency, transactionItem.currency) &&
        Objects.equals(this.description, transactionItem.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionId, operationId, accountingDate, valueDate, type, amount, currency, description);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionResponse {\n");
    
    sb.append("    transactionId: ").append(toIndentedString(transactionId)).append("\n");
    sb.append("    operationId: ").append(toIndentedString(operationId)).append("\n");
    sb.append("    accountingDate: ").append(toIndentedString(accountingDate)).append("\n");
    sb.append("    valueDate: ").append(toIndentedString(valueDate)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
