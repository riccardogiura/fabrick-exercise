package com.fabrick.account.common.model;

import java.util.List;

public class TransactionResponse {
    private List<TransactionItem> list;

    public List<TransactionItem> getList() {
        return list;
    }
}
