package com.fabrick.account.dto;

import com.fabrick.account.common.model.CreateMoneyTransferRequest;

public class CreateMoneyTransferRequestDto {
    private String accountId;
    private CreateMoneyTransferRequest request;

    public CreateMoneyTransferRequestDto(String accountId, CreateMoneyTransferRequest request) {
        this.accountId = accountId;
        this.request = request;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public CreateMoneyTransferRequest getRequest() {
        return request;
    }

    public void setRequest(CreateMoneyTransferRequest request) {
        this.request = request;
    }
}
