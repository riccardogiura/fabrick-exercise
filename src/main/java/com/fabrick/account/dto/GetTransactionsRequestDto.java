package com.fabrick.account.dto;

public class GetTransactionsRequestDto {
    private String accountId;
    private String fromAccountingDate;
    private String toAccountingDate;

    public GetTransactionsRequestDto(String accountId, String fromAccountingDate, String toAccountingDate) {
        this.accountId = accountId;
        this.fromAccountingDate = fromAccountingDate;
        this.toAccountingDate = toAccountingDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFromAccountingDate() {
        return fromAccountingDate;
    }

    public void setFromAccountingDate(String fromAccountingDate) {
        this.fromAccountingDate = fromAccountingDate;
    }

    public String getToAccountingDate() {
        return toAccountingDate;
    }

    public void setToAccountingDate(String toAccountingDate) {
        this.toAccountingDate = toAccountingDate;
    }
}
