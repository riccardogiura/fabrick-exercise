package com.fabrick.account.controller.impl;

import com.fabrick.account.common.client.AccountApiFacade;
import com.fabrick.account.common.itf.Operation;
import com.fabrick.account.common.model.BalanceResponse;
import com.fabrick.account.common.model.TransactionItem;
import com.fabrick.account.controller.api.AccountApi;
import com.fabrick.account.dto.GetTransactionsRequestDto;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CashAccountApiController implements AccountApi {

    @Autowired
    @Qualifier("getBalanceServiceBean")
    private Operation<String, BalanceResponse, Exception> getBalanceServiceBean;

    @Autowired
    @Qualifier("getTransactionServiceBean")
    private Operation<GetTransactionsRequestDto, List<TransactionItem>, Exception> getTransactionServiceBean;

    public ResponseEntity<BalanceResponse> getCashAccountBalance(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId) throws Exception {
        return new ResponseEntity<>(getBalanceServiceBean.execute(accountId), HttpStatus.OK);
    }

    public ResponseEntity<List<TransactionItem>> getCashAccountTransactions(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId,
            @Parameter(in = ParameterIn.QUERY, description = "from accounting date", schema = @Schema()) @Valid @RequestParam(value = "fromAccountingDate", required = true) String fromAccountingDate,
            @Parameter(in = ParameterIn.QUERY, description = "to accounting date", schema = @Schema()) @Valid @RequestParam(value = "toAccountingDate", required = true) String toAccountingDate) throws Exception {
        GetTransactionsRequestDto dto = new GetTransactionsRequestDto(accountId, fromAccountingDate, toAccountingDate);
        return new ResponseEntity<>(getTransactionServiceBean.execute(dto), HttpStatus.OK);
    }

}
