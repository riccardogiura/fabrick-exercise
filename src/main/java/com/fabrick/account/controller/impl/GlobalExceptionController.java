
package com.fabrick.account.controller.impl;


import com.fabrick.account.common.model.ApiError;
import com.fabrick.account.common.model.fabrickresponse.FabrickApiError;
import com.fabrick.account.common.model.fabrickresponse.FabrickResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.ArrayList;
import java.util.List;


@ControllerAdvice
public class GlobalExceptionController {

    //400
    @ExceptionHandler({HttpClientErrorException.BadRequest.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<ApiError> badRequestErrorHandler(HttpClientErrorException e) {
        return buildResponse(HttpStatus.BAD_REQUEST, e);
    }

    //401
    @ExceptionHandler(HttpClientErrorException.Unauthorized.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ResponseEntity<ApiError> unauthorizedErrorHandler(HttpClientErrorException e) {
        return buildResponse(HttpStatus.UNAUTHORIZED, e);
    }

    //403
    @ExceptionHandler(HttpClientErrorException.Forbidden.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ResponseEntity<ApiError> forbiddenErrorHandler(HttpClientErrorException e) {
        return buildResponse(HttpStatus.FORBIDDEN, e);
    }

    //404
    @ExceptionHandler(HttpClientErrorException.NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseEntity<ApiError> notFoundErrorHandler(HttpClientErrorException e) {
        return buildResponse(HttpStatus.NOT_FOUND, e);

    }

    //500
    @ExceptionHandler(HttpServerErrorException.InternalServerError.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseEntity<ApiError> genericErrorHandler(HttpServerErrorException e) {
        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }


    @ExceptionHandler(HttpServerErrorException.BadGateway.class)
    @ResponseStatus(HttpStatus.BAD_GATEWAY)
    @ResponseBody
    public ResponseEntity<ApiError> badGatewayErrorHandler(HttpServerErrorException e) {
        return buildResponse(HttpStatus.BAD_GATEWAY, e);
    }

    @ExceptionHandler(HttpServerErrorException.GatewayTimeout.class)
    @ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
    @ResponseBody
    public ResponseEntity<ApiError> gatewayTimeOutErrorHandler(HttpServerErrorException e) {
        return buildResponse(HttpStatus.GATEWAY_TIMEOUT, e);
    }

    @ExceptionHandler(HttpServerErrorException.ServiceUnavailable.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ResponseBody
    public ResponseEntity<ApiError> serviceUnavailableErrorHandler(HttpServerErrorException e) {
        return buildResponse(HttpStatus.SERVICE_UNAVAILABLE, e);
    }

    private ResponseEntity<ApiError> buildResponse(HttpStatus status, HttpStatusCodeException e) {
        FabrickResponse fabrickApiResponse = e.getResponseBodyAs(FabrickResponse.class);
        List<FabrickApiError> errors = null != fabrickApiResponse ? fabrickApiResponse.getErrors() : new ArrayList<>();
        return new ResponseEntity<>(new ApiError(
                status,
                errors), status);
    }

}
