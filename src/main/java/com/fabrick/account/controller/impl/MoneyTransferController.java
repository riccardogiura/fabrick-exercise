package com.fabrick.account.controller.impl;

import com.fabrick.account.common.client.AccountApiFacade;
import com.fabrick.account.common.itf.Operation;
import com.fabrick.account.common.model.CreateMoneyTransferRequest;
import com.fabrick.account.common.model.CreateMoneyTransferResponse;
import com.fabrick.account.common.model.fabrickresponse.FabrickCreateMoneyTransferResponse;
import com.fabrick.account.controller.api.MoneyTransferApi;
import com.fabrick.account.dto.CreateMoneyTransferRequestDto;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MoneyTransferController implements MoneyTransferApi {

    @Autowired
    @Qualifier("createMoneyTransferServiceBean")
    private Operation<CreateMoneyTransferRequestDto, CreateMoneyTransferResponse, Exception> createMoneyTransferServiceBean;

    public ResponseEntity<CreateMoneyTransferResponse> createMoneyTransfer(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId,
            @Parameter(in = ParameterIn.DEFAULT) @Valid @RequestBody CreateMoneyTransferRequest body) throws Exception {
        CreateMoneyTransferRequestDto dto = new CreateMoneyTransferRequestDto(accountId, body);
        return new ResponseEntity<>(createMoneyTransferServiceBean.execute(dto), HttpStatus.CREATED);
    }

}
