
package com.fabrick.account.controller.api;

import com.fabrick.account.common.model.BalanceResponse;
import com.fabrick.account.common.model.CreateMoneyTransferRequest;
import com.fabrick.account.common.model.CreateMoneyTransferResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@Validated
@RequestMapping(value = "/account/{accountId}")
@RestController
public interface MoneyTransferApi {

    @Operation(summary = "Creates a new money transfer", description = "Creates a new money transfer for the given accountID", tags = {"Money Transfer"})
    @PostMapping(value = "/payments/money-transfers",
            produces = {"application/json"})
    ResponseEntity<CreateMoneyTransferResponse> createMoneyTransfer(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId,
            @Parameter(in = ParameterIn.DEFAULT) @Valid @RequestBody CreateMoneyTransferRequest body) throws Exception;

}

