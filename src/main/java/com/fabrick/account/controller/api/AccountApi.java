
package com.fabrick.account.controller.api;

import com.fabrick.account.common.model.BalanceResponse;
import com.fabrick.account.common.model.TransactionItem;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Validated
@RequestMapping(value = "/account/{accountId}")
@RestController
public interface AccountApi {

    @Operation(summary = "returns cash account balance", description = "By passing in the account ID, it retrieves the balance of that cash account ", tags = {"Cash Account"})
    @GetMapping(value = "/balance",
            produces = {"application/json"})
    ResponseEntity<BalanceResponse> getCashAccountBalance(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId) throws Exception;

    @Operation(summary = "returns cash account transactions", description = "By passing in the account ID and two dates, it returns the list of transactions made between those dates", tags = {"Cash Account"})
    @GetMapping(value = "/transactions",
            produces = {"application/json"})
    ResponseEntity<List<TransactionItem>> getCashAccountTransactions(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId,
            @Parameter(in = ParameterIn.QUERY, description = "from accounting date", schema = @Schema()) @Valid @RequestParam(value = "fromAccountingDate", required = false) String fromAccountingDate,
            @Parameter(in = ParameterIn.QUERY, description = "to accounting date", schema = @Schema()) @Valid @RequestParam(value = "toAccountingDate", required = false) String toAccountingDate) throws Exception;
}

