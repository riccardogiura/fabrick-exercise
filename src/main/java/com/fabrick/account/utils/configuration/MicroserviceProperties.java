
package com.fabrick.account.utils.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service(value = "microserviceProperties")
public class MicroserviceProperties {


    @Value("${fabrick.balance-uri}")
    private String fabrickBalanceUri;

    @Value("${fabrick.transactions-uri}")
    private String fabrickTransactionsUri;

    @Value("${fabrick.money-transfers-uri}")
    private String fabrickMoneyTransfersUri;

    @Value("${fabrick.api-key}")
    private String fabrickApiKey;

    @Value("${fabrick.auth-schema}")
    private String fabrickAuthSchema;

    @Value("${fabrick.time-zone}")
    private String fabrickTimeZone;

    public String getFabrickBalanceUri() {
        return fabrickBalanceUri;
    }

    public String getFabrickApiKey() {
        return fabrickApiKey;
    }

    public String getFabrickAuthSchema() {
        return fabrickAuthSchema;
    }

    public String getFabrickTransactionsUri() {
        return fabrickTransactionsUri;
    }

    public String getFabrickMoneyTransfersUri() {
        return fabrickMoneyTransfersUri;
    }

    public String getFabrickTimeZone() {
        return fabrickTimeZone;
    }
}

