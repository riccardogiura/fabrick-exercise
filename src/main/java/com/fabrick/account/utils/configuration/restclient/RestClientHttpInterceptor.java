package com.fabrick.account.utils.configuration.restclient;

import com.fabrick.account.utils.configuration.Converter;
import com.fabrick.account.utils.configuration.MicroserviceProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map.Entry;


public class RestClientHttpInterceptor implements ClientHttpRequestInterceptor {
	private static final Logger log = LoggerFactory.getLogger(RestClientHttpInterceptor.class);

	@Autowired
	private MicroserviceProperties microserviceProperties;

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		logRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		logResponse(response);
		return response;
	}

	private void logRequest(HttpRequest request, byte[] body) {
		StringBuilder sb= new StringBuilder("===================================request begin========================================================"
				+ "\n\r URI         : ");
		sb.append(request.getURI().getPath());
		sb.append("\n\r Method      : ");
		sb.append(request.getMethod());
		if(StringUtils.isNotBlank(request.getURI().getQuery())){
			sb.append("\n\r QueryParams     : [");
			sb.append(request.getURI().getQuery());
			sb.append("]");
		}
		sb.append("\n\r Headers     : ");
		sb.append(headersToString(request.getHeaders()));
		if(body != null){
			sb.append("\n\r Request Body: ");
			sb.append(Converter.bytesToString(body));
		}
		sb.append("\n\r ==================================request end========================================================");
		log.info(sb.toString());

	}

	private void logResponse(ClientHttpResponse response) throws IOException {
		StringBuilder inputStringBuilder = new StringBuilder();
		try {
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(response.getBody(), Converter.getCharset()));
			String line = bufferedReader.readLine();
			while (line != null) {
				inputStringBuilder.append(line);
				inputStringBuilder.append("\n\r ");
				line = bufferedReader.readLine();
			}
		} catch (IOException e) {
			log.error("Error while logging response. Cause is: " + e.getLocalizedMessage());
		}


		log.info(
				"====================================response begin=================================================="
						+ "\n\r Status code  : {}" + "\n\r Status text  : {}" + "\n\r Headers      : {}" + "\n\r Response body: {}"
						+ "\n\r ===============================response end=========================================================",
				response.getStatusCode(), response.getStatusText(),
				response.getHeaders(), inputStringBuilder);

	}

	private String headersToString(HttpHeaders headers) {
		StringBuilder builder = new StringBuilder();
		for (Entry<String, List<String>> entry : headers.entrySet()) {
			builder.append(entry.getKey()).append("=[");
			for (String value : entry.getValue()) {
				builder.append(value).append(",");
			}
			builder.setLength(builder.length() - 1); // Get rid of trailing comma
			builder.append("],");
		}
		builder.setLength(builder.length() - 1); // Get rid of trailing comma
		return builder.toString();
	}

}
