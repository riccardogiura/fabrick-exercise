package com.fabrick.account.utils.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Base64Utils;

import javax.sql.rowset.serial.SerialBlob;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Converter {

	private static final Logger log = LoggerFactory.getLogger(Converter.class);

	protected Converter() {
	}

	public static Charset getCharset() {
		return StandardCharsets.UTF_8;
	}

	// byte to String
	public static String bytesToString(byte[] bs) {
		return new String(bs, getCharset());
	}

	// String to Byte
	public static byte[] stringToBytes(String in) {
		return in.getBytes(getCharset());
	}

	public static Timestamp stringToTimestamp(String timestampAsString) {
		String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

		ZonedDateTime localDateTime = ZonedDateTime.parse(timestampAsString, formatter);

		return Timestamp.valueOf(localDateTime.toLocalDateTime());
	}

	public static String timestampToString(Timestamp timestamp) {
		String pattern = "yyyy-MM-dd HH:mm:ss.SSS";
		Date date = new Date();
		date.setTime(timestamp.getTime());
		return new SimpleDateFormat(pattern).format(date);
	}

	public static String clobStringConversion(Clob clb) {
		if (clb == null)
			return "";

		StringBuilder str = new StringBuilder();
		String strng;

		BufferedReader bufferRead;
		try {
			bufferRead = new BufferedReader(clb.getCharacterStream());
			while ((strng = bufferRead.readLine()) != null)
				str.append(strng);
		} catch (IOException | SQLException e) {
			log.error("Error while converting clob to String ", e);

		}

		return str.toString();
	}

	public static String blobToBase64String(Blob blob) {
		String base64String = null;
		byte[] bytes;
		try {
			bytes = blob.getBytes(1l, (int) blob.length());
			base64String = Base64Utils.encodeToUrlSafeString(bytes);
		} catch (SQLException e) {
			log.error("Error while converting blob to String ", e);

		}
		return base64String;
	}

	public static Blob base64StringToBlob(String base64String) {
		try {
			return new SerialBlob(Base64Utils.decodeFromUrlSafeString(base64String));
		} catch (Exception e) {
			log.error("Error converting base64String to Blob");
		}
		return null;
	}
}
