package com.fabrick.account;

import com.fabrick.account.common.model.BalanceResponse;
import com.fabrick.account.common.model.TransactionItem;
import com.fabrick.account.common.model.TransactionType;
import com.fabrick.account.controller.api.AccountApi;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class BankingAccountApplicationTests {

    @Autowired
    private MockMvc mockMvc; // MockMvc per simulare le richieste HTTP

    @MockBean
    private AccountApi accountApi; // Mock del controller

    @Test
    public void testGetCashAccountBalance() throws Exception {
        // Dati di esempio
        String accountId = "123";
        BalanceResponse expectedResponse = new BalanceResponse();

        expectedResponse.setCurrency("EUR");
        expectedResponse.setBalance(new BigDecimal("29.64"));
        expectedResponse.setAvailableBalance(new BigDecimal("29.64"));
        expectedResponse.setDate(LocalDate.parse("2023-08-01"));

        // Configura il comportamento del mock del controller
        Mockito.when(accountApi.getCashAccountBalance(accountId)).thenReturn(new ResponseEntity<>(expectedResponse, HttpStatus.OK));

        // Esegui la richiesta GET al controller e verifica la risposta
        mockMvc.perform(get("/account/{accountId}/balance", accountId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currency", is(expectedResponse.getCurrency())))
                .andExpect(jsonPath("$.balance", is(expectedResponse.getBalance().doubleValue())))
                .andExpect(jsonPath("$.availableBalance", is(expectedResponse.getAvailableBalance().doubleValue())));
    }

    @Test
    public void testGetCashAccountTransactions() throws Exception {

        String accountId = "00000";
        String fromAccountingDate = "2019-07-01";
        String toAccountingDate = "2019-12-31";
        List<TransactionItem> expectedTransactions = generateTrItems();

        Mockito.when(accountApi.getCashAccountTransactions(accountId, fromAccountingDate, toAccountingDate))
                .thenReturn(new ResponseEntity<>(expectedTransactions, HttpStatus.OK));

        mockMvc.perform(get("/account/{accountId}/transactions", accountId)
                        .param("fromAccountingDate", fromAccountingDate)
                        .param("toAccountingDate", toAccountingDate))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].transactionId", is(expectedTransactions.get(0).getTransactionId())))
                .andExpect(jsonPath("$[0].operationId", is(expectedTransactions.get(0).getOperationId())));
    }

    private List<TransactionItem> generateTrItems() {
        List<TransactionItem> list = new ArrayList<>();

        TransactionItem transactionItem1 = new TransactionItem();
        transactionItem1.setTransactionId("1329140278001");
        transactionItem1.setOperationId("19000017094683");
        transactionItem1.setAccountingDate(LocalDate.parse("2019-02-01"));
        transactionItem1.setValueDate(LocalDate.parse("2019-02-01"));
        TransactionType type1 = new TransactionType();
        type1.setEnumeration("GBS_TRANSACTION_TYPE");
        type1.setValue("GBS_ACCOUNT_TRANSACTION_TYPE_0034");
        transactionItem1.setType(type1);
        transactionItem1.setAmount(new BigDecimal("89.00"));
        transactionItem1.setCurrency("EUR");
        transactionItem1.setDescription("GC LUCA TERRIBILE DA 03268.22300 DATA ORDINE 01022019 COPERTURA SPESE");

        TransactionItem transactionItem2 = new TransactionItem();
        transactionItem2.setTransactionId("398894");
        transactionItem2.setOperationId("00000000398894");
        transactionItem2.setAccountingDate(LocalDate.parse("2019-01-31"));
        transactionItem2.setValueDate(LocalDate.parse("2019-02-01"));
        TransactionType type2 = new TransactionType();
        type2.setEnumeration("GBS_TRANSACTION_TYPE");
        type2.setValue("GBS_ACCOUNT_TRANSACTION_TYPE_0050");
        transactionItem2.setType(type2);
        transactionItem2.setAmount(new BigDecimal("-95.73"));
        transactionItem2.setCurrency("EUR");
        transactionItem2.setDescription("PD VISA CORPORATE 12");

        list.add(transactionItem1);
        list.add(transactionItem2);

        return list;
    }


}
