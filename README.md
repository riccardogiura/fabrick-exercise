# Fabrick Exercise

## Description

This repository contains the source code for the Fabrick Exercise. In order to test the application, please follow the instructions below.

## Prerequisites

Make sure you have the following software installed on your system:

1. Git
2. Java Development Kit (JDK) - OpenJDK17 
3. Maven version 3.8.1 or above

## Getting Started

To get started with testing the application, follow these steps:

1. Clone the repository

2. Navigate to the project directory

3. Build the project using Maven:
```bash
mvn clean install
```
4. After a successful build, you can find the executable jar file in the target directory.

5. Run the application using the following command, replacing <path-to-jar> with the actual path to the jar file. Be sure to use OpenJDK 17. 
```bash
java -jar <path-to-jar>
```

## Usage

Once the application is running, you can access the Swagger Web UI through your browser at [Swagger UI](http://localhost:8080/banking/v1/swagger-ui/index.html). 
You can also use API clients like Postman to interact with its endpoints.


